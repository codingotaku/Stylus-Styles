# Stylus-Styles

Bunch of personal styles for the [Stylus extension](https://addons.mozilla.org/en-US/firefox/addon/styl-us/)

## Currently contains
- [Kitsu.io](https://codeberg.org/codingotaku/Stylus-Styles/raw/branch/main/kitsu.io.css)
- [outlook.office(365).com](https://codeberg.org/codingotaku/Stylus-Styles/raw/branch/main/outlook-office365.css)


## How To Use the Styles?
Please read the [official Stylus guide](https://github.com/openstyles/stylus/wiki/FAQ#how-can-i-export-my-user-styles-from-stylish-for-firefox-to-stylus)